describe('ventana principal', () => {
  it('tiene encabezado correcto y en español por defecto', () => {    //* testea que el encabezado este correcto
    cy.visit('http://localhost:4200');   //* testea que la visita al sitio sea por el puerto de acceso localhost:4200
    cy.contains('angular-wishlist');    //* testea que el contenido diga angular-wishlist
    cy.get('h1 b').should('contain', 'HOLA es');   //* testea que exista h1, que haya una b, y que diga HOLA es
  });
});
