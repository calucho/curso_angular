import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReservasApiClientService } from './reservas-api-client.service';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasRoutingModule } from './reservas-routing.module';


@NgModule({
  declarations: [ReservasListadoComponent, ReservasDetalleComponent],
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],

  providers: [
    ReservasApiClientService
  ]

})

export class ReservasModule { }





