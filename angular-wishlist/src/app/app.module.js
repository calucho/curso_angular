"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AppModule = exports.db = exports.MyDatabase = exports.Translation = exports.init_app = exports.APP_CONFIG = exports.childrenRoutesVuelos = void 0;
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
// Importamos el modulo de Ruteo
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var destino_viaje_component_1 = require("./components/destino-viaje/destino-viaje.component");
var lista_destinos_component_1 = require("./components/lista-destinos/lista-destinos.component");
var destino_detalle_component_1 = require("./components/destino-detalle/destino-detalle.component");
// Agregamos un nuevo import para manejos de formularios
var forms_1 = require("@angular/forms");
var form_destino_viaje_component_1 = require("./components/form-destino-viaje/form-destino-viaje.component");
var destinos_api_client_model_1 = require("./models/destinos-api-client.model");
// Importamos para redux
var store_1 = require("@ngrx/store");
var effects_1 = require("@ngrx/effects");
var store_devtools_1 = require("@ngrx/store-devtools");
var destinos_viajes_state_model_1 = require("./models/destinos-viajes-state.model");
var login_component_1 = require("./components/login/login/login.component");
var protected_component_1 = require("./components/protected/protected/protected.component");
var auth_service_1 = require("./services/auth.service");
var usuario_logueado_guard_1 = require("./guards/usuario-logueado/usuario-logueado.guard");
var vuelos_component_component_1 = require("./components/vuelos/vuelos-component/vuelos-component.component");
var vuelos_main_component_component_1 = require("./components/vuelos/vuelos-main-component/vuelos-main-component.component");
var vuelos_mas_info_component_component_1 = require("./components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component");
var vuelos_detalle_component_component_1 = require("./components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component");
var reservas_module_1 = require("./reservas/reservas.module");
var http_1 = require("@angular/common/http");
var dexie_1 = require("dexie");
var core_2 = require("@ngx-translate/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
// Importamos el Mapa
var ngx_mapbox_gl_1 = require("ngx-mapbox-gl");
// fin Mapa
// Importamos las Animaciones
var animations_1 = require("@angular/platform-browser/animations");
var espiame_directive_1 = require("./espiame.directive");
var trackear_click_directive_1 = require("./trackear-click.directive");
// Fin Animaciones
// init routing
exports.childrenRoutesVuelos = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'main', component: vuelos_main_component_component_1.VuelosMainComponentComponent },
    { path: 'mas-info', component: vuelos_mas_info_component_component_1.VuelosMasInfoComponentComponent },
    { path: ':id', component: vuelos_detalle_component_component_1.VuelosDetalleComponent },
];
// Declaramos las direcciones den nav
var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: lista_destinos_component_1.ListaDestinosComponent },
    { path: 'destino/:id', component: destino_detalle_component_1.DestinoDetalleComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'protected', component: protected_component_1.ProtectedComponent, canActivate: [usuario_logueado_guard_1.UsuarioLogueadoGuard] },
    { path: 'vuelos', component: vuelos_component_component_1.VuelosComponentComponent, canActivate: [usuario_logueado_guard_1.UsuarioLogueadoGuard],
        children: exports.childrenRoutesVuelos }
];
var reducers = {
    destinos: destinos_viajes_state_model_1.reducerDestinosViajes
};
var reducersInitialState = {
    destinos: destinos_viajes_state_model_1.initializeDestinosViajesState()
};
var APP_CONFIG_VALUE = {
    apiEndpoint: "http://localhost:3000"
};
exports.APP_CONFIG = new core_1.InjectionToken('app.config');
// Fin app config
// app init
function init_app(appLoadService) {
    return function () { return appLoadService.initializeDestinosViajesState(); };
}
exports.init_app = init_app;
var AppLoadService = /** @class */ (function () {
    function AppLoadService(store, http) {
        this.store = store;
        this.http = http;
    }
    AppLoadService.prototype.initializeDestinosViajesState = function () {
        return __awaiter(this, void 0, void 0, function () {
            var headers, req, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        headers = new http_1.HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
                        req = new http_1.HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
                        return [4 /*yield*/, this.http.request(req).toPromise()];
                    case 1:
                        response = _a.sent();
                        this.store.dispatch(new destinos_viajes_state_model_1.InitMyDataAction(response.body));
                        return [2 /*return*/];
                }
            });
        });
    };
    AppLoadService = __decorate([
        core_1.Injectable()
    ], AppLoadService);
    return AppLoadService;
}());
// FIN app init
// Dexie db
var Translation = /** @class */ (function () {
    function Translation(id, lang, key, value) {
        this.id = id;
        this.lang = lang;
        this.key = key;
        this.value = value;
    }
    return Translation;
}());
exports.Translation = Translation;
var MyDatabase = /** @class */ (function (_super) {
    __extends(MyDatabase, _super);
    function MyDatabase() {
        var _this = _super.call(this, 'MyDatabase') || this;
        _this.version(1).stores({
            destinos: '++id, nombre, imagenUrl'
        });
        _this.version(2).stores({
            destinos: '++id, nombre, imagenUrl',
            translations: '++id, lang, key, value'
        });
        return _this;
    }
    MyDatabase = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], MyDatabase);
    return MyDatabase;
}(dexie_1["default"]));
exports.MyDatabase = MyDatabase;
exports.db = new MyDatabase();
// FIN dexie db
// i18n ini
var TranslationLoader = /** @class */ (function () {
    function TranslationLoader(http) {
        this.http = http;
    }
    TranslationLoader.prototype.getTranslation = function (lang) {
        var _this = this;
        var promise = exports.db.translations
            .where('lang')
            .equals(lang)
            .toArray()
            .then(function (results) {
            if (results.length === 0) {
                return _this.http
                    .get(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                    .toPromise()
                    .then(function (apiResults) {
                    exports.db.translations.bulkAdd(apiResults);
                    return apiResults;
                });
            }
            return results;
        }).then(function (traducciones) {
            console.log('traducciones cargadas:');
            console.log(traducciones);
            return traducciones;
        }).then(function (traducciones) {
            return traducciones.map(function (t) {
                var _a;
                return (_a = {}, _a[t.key] = t.value, _a);
            });
        });
        return rxjs_1.from(promise).pipe(operators_1.flatMap(function (elems) { return rxjs_1.from(elems); }));
    };
    return TranslationLoader;
}());
function HttpLoaderFactory(http) {
    return new TranslationLoader(http);
}
// fin i18n
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                destino_viaje_component_1.DestinoViajeComponent,
                lista_destinos_component_1.ListaDestinosComponent,
                destino_detalle_component_1.DestinoDetalleComponent,
                form_destino_viaje_component_1.FormDestinoViajeComponent,
                login_component_1.LoginComponent,
                protected_component_1.ProtectedComponent,
                vuelos_component_component_1.VuelosComponentComponent,
                vuelos_main_component_component_1.VuelosMainComponentComponent,
                vuelos_mas_info_component_component_1.VuelosMasInfoComponentComponent,
                vuelos_detalle_component_component_1.VuelosDetalleComponent,
                espiame_directive_1.EspiameDirective,
                trackear_click_directive_1.TrackearClickDirective
            ],
            imports: [
                platform_browser_1.BrowserModule,
                //Agregamos Forms Module y Reactive
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                //Se cargan las rutas
                router_1.RouterModule.forRoot(routes),
                http_1.HttpClientModule,
                //Se carga redux
                store_1.StoreModule.forRoot(reducers, { initialState: reducersInitialState,
                    runtimeChecks: {
                        strictStateImmutability: false,
                        strictActionImmutability: false
                    }
                }),
                store_devtools_1.StoreDevtoolsModule.instrument(),
                effects_1.EffectsModule.forRoot([destinos_viajes_state_model_1.DestinosViajesEffects]),
                reservas_module_1.ReservasModule,
                core_2.TranslateModule.forRoot({
                    loader: {
                        provide: core_2.TranslateLoader,
                        useFactory: (HttpLoaderFactory),
                        deps: [http_1.HttpClient]
                    }
                }),
                ngx_mapbox_gl_1.NgxMapboxGLModule,
                animations_1.BrowserAnimationsModule,
            ],
            providers: [
                auth_service_1.AuthService,
                MyDatabase,
                destinos_api_client_model_1.DestinosApiClient,
                usuario_logueado_guard_1.UsuarioLogueadoGuard,
                { provide: exports.APP_CONFIG, useValue: APP_CONFIG_VALUE },
                AppLoadService,
                { provide: core_1.APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
