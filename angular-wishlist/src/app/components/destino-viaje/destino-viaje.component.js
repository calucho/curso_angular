"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DestinoViajeComponent = void 0;
var core_1 = require("@angular/core");
var destinos_viajes_state_model_1 = require("./../../models/destinos-viajes-state.model");
// Se importan las animaciones
var animations_1 = require("@angular/animations");
var DestinoViajeComponent = /** @class */ (function () {
    function DestinoViajeComponent(store) {
        this.store = store;
        // @Input('idx') position: number;  //No es muy recomendable renombrar las variables asi
        this.cssClass = 'col-md-4';
        this.clicked = new core_1.EventEmitter();
    }
    DestinoViajeComponent.prototype.ngOnInit = function () {
    };
    DestinoViajeComponent.prototype.ir = function () {
        this.clicked.emit(this.destino);
        return false;
    };
    DestinoViajeComponent.prototype.voteUp = function () {
        this.store.dispatch(new destinos_viajes_state_model_1.VoteUpAction(this.destino));
        return false;
    };
    DestinoViajeComponent.prototype.voteDown = function () {
        this.store.dispatch(new destinos_viajes_state_model_1.VoteDownAction(this.destino));
        return false;
    };
    __decorate([
        core_1.Input()
    ], DestinoViajeComponent.prototype, "destino");
    __decorate([
        core_1.Input()
    ], DestinoViajeComponent.prototype, "position");
    __decorate([
        core_1.HostBinding('attr.class')
    ], DestinoViajeComponent.prototype, "cssClass");
    __decorate([
        core_1.Output()
    ], DestinoViajeComponent.prototype, "clicked");
    DestinoViajeComponent = __decorate([
        core_1.Component({
            selector: 'app-destino-viaje',
            templateUrl: './destino-viaje.component.html',
            styleUrls: ['./destino-viaje.component.css'],
            animations: [
                animations_1.trigger('esFavorito', [
                    animations_1.state('estadoFavorito', animations_1.style({
                        backgroundColor: 'PaleTurquoise'
                    })),
                    animations_1.state('estadoNoFavorito', animations_1.style({
                        backgroundColor: 'WhiteSmoke'
                    })),
                    animations_1.transition('estadoNoFavorito => estadoFavorito', [
                        animations_1.animate('3s')
                    ]),
                    animations_1.transition('estadoFavorito => estadoNoFavorito', [
                        animations_1.animate('1s')
                    ]),
                ])
            ]
        })
    ], DestinoViajeComponent);
    return DestinoViajeComponent;
}());
exports.DestinoViajeComponent = DestinoViajeComponent;
