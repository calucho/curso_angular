import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';
import { DestinoViaje } from './../../models/destino-viaje.model';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})

export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;  // cantidad de caracteres minimos designada para validar el nombre
  searchResults: [];


  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    // inicializar
    this.onItemAdded = new EventEmitter();
    // vinculacion con tag html
    this.fg = fb.group({
      // requiere estar cargado un nombre en la caja nombre
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],

      url: ['']
    });

    // observador de tipeo
    // this.fg.valueChanges.subscribe((form: any) => {
    //   console.log('cambió el formulario: ', form);
    // });

  }


  // para detectar las sugerencias a medida que vamos tipeando en la caja de texto "nombre"
  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
          map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
          filter(text => text.length > 2),
          debounceTime(120),
          distinctUntilChanged(),
          switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
        ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  // para ejecutar la lógica de los validadores
  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    // si en la caja el nombre es menor a 5 caracteres no se activa "guardar"
    if (l > 0 && l < 3) {      // la l (ele) equivale a la longitud de los caracteres en la caja nombre
      return { invalidNombre: true };
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) { // la l (ele) equivale a la longitud de los caracteres en la caja nombre
        return { minLongNombre: true };
      }
      return null;
    };
  }


}


















































