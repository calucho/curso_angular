"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.FormDestinoViajeComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var ajax_1 = require("rxjs/ajax");
var operators_1 = require("rxjs/operators");
var app_module_1 = require("src/app/app.module");
var destino_viaje_model_1 = require("./../../models/destino-viaje.model");
var FormDestinoViajeComponent = /** @class */ (function () {
    function FormDestinoViajeComponent(fb, config) {
        this.config = config;
        this.minLongitud = 3; // cantidad de caracteres minimos designada para validar el nombre
        // inicializar
        this.onItemAdded = new core_1.EventEmitter();
        // vinculacion con tag html
        this.fg = fb.group({
            // requiere estar cargado un nombre en la caja nombre
            nombre: ['', forms_1.Validators.compose([
                    forms_1.Validators.required,
                    this.nombreValidator,
                    this.nombreValidatorParametrizable(this.minLongitud)
                ])],
            url: ['']
        });
        // observador de tipeo
        // this.fg.valueChanges.subscribe((form: any) => {
        //   console.log('cambió el formulario: ', form);
        // });
    }
    // para detectar las sugerencias a medida que vamos tipeando en la caja de texto "nombre"
    FormDestinoViajeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemNombre = document.getElementById('nombre');
        rxjs_1.fromEvent(elemNombre, 'input')
            .pipe(operators_1.map(function (e) { return e.target.value; }), operators_1.filter(function (text) { return text.length > 2; }), operators_1.debounceTime(120), operators_1.distinctUntilChanged(), operators_1.switchMap(function (text) { return ajax_1.ajax(_this.config.apiEndpoint + '/ciudades?q=' + text); })).subscribe(function (ajaxResponse) { return _this.searchResults = ajaxResponse.response; });
    };
    FormDestinoViajeComponent.prototype.guardar = function (nombre, url) {
        var d = new destino_viaje_model_1.DestinoViaje(nombre, url);
        this.onItemAdded.emit(d);
        return false;
    };
    // para ejecutar la lógica de los validadores
    FormDestinoViajeComponent.prototype.nombreValidator = function (control) {
        var l = control.value.toString().trim().length;
        // si en la caja el nombre es menor a 5 caracteres no se activa "guardar"
        if (l > 0 && l < 3) { // la l (ele) equivale a la longitud de los caracteres en la caja nombre
            return { invalidNombre: true };
        }
        return null;
    };
    FormDestinoViajeComponent.prototype.nombreValidatorParametrizable = function (minLong) {
        return function (control) {
            var l = control.value.toString().trim().length;
            if (l > 0 && l < minLong) { // la l (ele) equivale a la longitud de los caracteres en la caja nombre
                return { minLongNombre: true };
            }
            return null;
        };
    };
    __decorate([
        core_1.Output()
    ], FormDestinoViajeComponent.prototype, "onItemAdded");
    FormDestinoViajeComponent = __decorate([
        core_1.Component({
            selector: 'app-form-destino-viaje',
            templateUrl: './form-destino-viaje.component.html',
            styleUrls: ['./form-destino-viaje.component.css']
        }),
        __param(1, core_1.Inject(core_1.forwardRef(function () { return app_module_1.APP_CONFIG; })))
    ], FormDestinoViajeComponent);
    return FormDestinoViajeComponent;
}());
exports.FormDestinoViajeComponent = FormDestinoViajeComponent;
