
import { v4 as uuid} from 'uuid';
export class DestinoViaje {

  //   nombre: string;
  //   imagenUrl: string;

  //   constructor(n: string, u: string) {
  //     this.nombre = n;
  //     this.imagenUrl = u;
  //   }


  // Las variables y su contructor arriba commented fue reemplazada con el siguiente codigo
  // cambiando las variables a publicas. Hace exactamente lo mismo



  // Para marcar el destino como elegido
  private selected: boolean;
  public servicios: string[];  // declaramos el Array servicios
  id = uuid();

  constructor(public nombre: string, public imagenUrl: string, public votes: number = 0 ) {
    this.servicios = ['piscina', 'desayuno', 'cena'];  // Iniciamos el Array servicios en el constructor

  }
  // Metodo para identificar si el destino esta seleccionado o no
  isSelected(): boolean {
    return this.selected;
  }
  // Metodo para marcar el destino como seleccionado
  // tslint:disable-next-line:typedef
  setSelected(s: boolean) {
    this.selected = s;
  }

  voteUp() {
    this.votes++;
  }

  voteDown() {
    this.votes--;
  }

}












