"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.DestinosApiClient = void 0;
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var app_module_1 = require("./../app.module");
var destinos_viajes_state_model_1 = require("./destinos-viajes-state.model");
var DestinosApiClient = /** @class */ (function () {
    function DestinosApiClient(store, config, http) {
        var _this = this;
        this.store = store;
        this.config = config;
        this.http = http;
        this.destinos = [];
        this.store
            .select(function (state) { return state.destinos; })
            .subscribe(function (data) {
            console.log('destinos sub store');
            console.log(data);
            _this.destinos = data.items;
        });
        this.store
            .subscribe(function (data) {
            console.log('all store');
            console.log(data);
        });
    }
    DestinosApiClient.prototype.add = function (d) {
        var _this = this;
        var headers = new http_1.HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
        var req = new http_1.HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
        this.http.request(req).subscribe(function (data) {
            if (data.status === 200) {
                _this.store.dispatch(new destinos_viajes_state_model_1.NuevoDestinoAction(d));
                var myDb = app_module_1.db;
                myDb.destinos.add(d);
                console.log('todos los destinos de la db!');
                myDb.destinos.toArray().then(function (destinos) { return console.log(destinos); });
            }
        });
    };
    DestinosApiClient.prototype.getById = function (id) {
        return this.destinos.filter(function (d) { return d.id.toString() === id; })[0];
    };
    DestinosApiClient.prototype.getAll = function () {
        return this.destinos;
    };
    DestinosApiClient.prototype.elegir = function (d) {
        // aqui invocariamos al servidor
        this.store.dispatch(new destinos_viajes_state_model_1.ElegidoFavoritoAction(d));
    };
    DestinosApiClient = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(core_1.forwardRef(function () { return app_module_1.APP_CONFIG; })))
    ], DestinosApiClient);
    return DestinosApiClient;
}());
exports.DestinosApiClient = DestinosApiClient;
