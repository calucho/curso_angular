"use strict";
exports.__esModule = true;
exports.DestinoViaje = void 0;
var uuid_1 = require("uuid");
var DestinoViaje = /** @class */ (function () {
    function DestinoViaje(nombre, imagenUrl, votes) {
        if (votes === void 0) { votes = 0; }
        this.nombre = nombre;
        this.imagenUrl = imagenUrl;
        this.votes = votes;
        this.id = uuid_1.v4();
        this.servicios = ['piscina', 'desayuno', 'cena']; // Iniciamos el Array servicios en el constructor
    }
    // Metodo para identificar si el destino esta seleccionado o no
    DestinoViaje.prototype.isSelected = function () {
        return this.selected;
    };
    // Metodo para marcar el destino como seleccionado
    // tslint:disable-next-line:typedef
    DestinoViaje.prototype.setSelected = function (s) {
        this.selected = s;
    };
    DestinoViaje.prototype.voteUp = function () {
        this.votes++;
    };
    DestinoViaje.prototype.voteDown = function () {
        this.votes--;
    };
    return DestinoViaje;
}());
exports.DestinoViaje = DestinoViaje;
